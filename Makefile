CFLAGS := -g -Wall -Wextra -Wshadow -Wpointer-arith -Wcast-qual -Wcast-align -Wconversion -Wundef -Wunused-result
CC ?= gcc
RM ?= rm -rfv
LINKFLAGS ?= -lwayland-client -lwayland-server -lwayland-egl -lEGL -lGLESv2
LDLIBS ?=

.PHONY: $(TARGET)
.PHONY: clean

# Path for .c , .h and .o Files
SRC_PATH ?= ./src/
OBJ_PATH ?= ./obj/
OUT_PATH ?= ./out
INC_PATH ?= -I ./include

# Executable Name
TARGET := main

# Files to compile
# ./src/main.c ./src/dlist.c
SOURCES_FILES := $(wildcard $(SRC_PATH)*.c)
OBJECTS_FILES := $(patsubst %.c, $(OBJ_PATH)%.o, $(SOURCES_FILES))

# main.o dlist.o
OBJECTS := $(notdir $(OBJECTS_FILES))

# obj/main.o obj/dlist.o
OBJ := $(patsubst %,$(OBJ_PATH)%,$(OBJECTS))

# Build .o first
# -c arg : 	Compile or assemble the source files, but do not link.
# 			The linking stage simply is not done.
# 			The ultimate output is in the form of an object
#			file for each source file.
$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@echo "[CC] $<"
	@$(CC) $(CFLAGS) $(LDLIBS) -o $@ -c $< $(INC_PATH)

# Build final Binary
$(TARGET): $(OBJ)
	@echo "[INFO] Creating Binary Executable [$(TARGET)]"
	@$(CC) -o $(OUT_PATH)/$@ $^ $(LINKFLAGS)

# Clean all the object files and the binary
clean:
	@echo "[CLEANING]"
	@$(RM) $(TARGET)
	@$(RM) $(OBJ_PATH)*.o

memory: $(TARGET)
	valgrind --leak-check=yes ./$(TARGET)

