#ifndef GL_UTILS_H
#define GL_UTILS_H

#include <wayland-client.h>
#include <wayland-server.h>
#include <wayland-client-protocol.h>
#include <wayland-egl.h> // Wayland EGL MUST be included before EGL headers

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <fcntl.h>    /* low-level i/o */
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <stdint.h>

#include <string.h>
#include <stdlib.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <EGL/eglplatform.h>
#include <GLES2/gl2.h>

void print_opengl_error(const char *file, int line);
int compile_shader(const char *FName, GLuint ShaderNum);
int load_shaders(const char *vShaderFName, const char *pShaderFName,
	GLuint *program_handle);
void print_config_attributes(EGLDisplay display, EGLConfig config);

#define PRINT_OGL_ERROR() print_opengl_error(__FILE__, __LINE__)

#endif // GL_UTILS_H
