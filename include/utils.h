#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <string.h>

#include <fcntl.h>    /* low-level i/o */
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <stdint.h>

#include <string.h>
#include <stdlib.h>

#define WINDOW_WIDTH                1280
#define WINDOW_HEIGHT               800

#define IMAGE_RESOLUTION            WINDOW_WIDTH * WINDOW_HEIGHT

void read_file(const char *filename, void *buffer);
void hex_dump(char *name_buffer, void *buffer, int len);

#endif // _UTILS_H_
