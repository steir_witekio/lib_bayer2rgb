precision highp float;

varying vec2 opos;
uniform sampler2D BayerTex;

void main(void) {
	vec2 alternate;
	float r,g,b;

	alternate.x = mod(floor(opos.x * 1280.0), 2.0);
	alternate.y = mod(floor(opos.y * 800.0), 2.0);

	if (floor(alternate.x) == 0.0) {
		// Blue
		if (floor(alternate.y) == 0.0) {
			r = texture2D(BayerTex, vec2(opos.x + (1.0/1280.0), opos.y + (1.0/800.0))).r;
			g = (texture2D(BayerTex, vec2(opos.x + (1.0/1280.0), opos.y)).r + texture2D(BayerTex, vec2(opos.x, opos.y + (1.0/800.0))).r) / 2.0;
			b = texture2D(BayerTex, opos).r;
		}
		// Gr
		else {
			r = texture2D(BayerTex, vec2(opos.x + (1.0/1280.0), opos.y)).r;
			g = texture2D(BayerTex, opos).r;
			b = texture2D(BayerTex, vec2(opos.x, opos.y - (1.0/800.0))).r;
		}
	}
	else {
		// Gb
		if (floor(alternate.y) == 0.0) {
			r = texture2D(BayerTex, vec2(opos.x, opos.y + (1.0/800.0))).r;
			g = texture2D(BayerTex, opos).r;
			b = texture2D(BayerTex, vec2(opos.x - (1.0/1280.0), opos.y)).r;
		}
		// Red
		else {
			r = texture2D(BayerTex, opos).r;
			g = (texture2D(BayerTex, vec2(opos.x - (1.0/1280.0), opos.y)).r + texture2D(BayerTex, vec2(opos.x, opos.y - (1.0/800.0))).r) / 2.0;
			b = texture2D(BayerTex, vec2(opos.x - (1.0/1280.0), opos.y - (1.0/800.0))).r;
		}
	}

	// gl_FragColor = texture2D(BayerTex, vec2(opos.x, opos.y));
	gl_FragColor=vec4(r,g,b,1);
}

