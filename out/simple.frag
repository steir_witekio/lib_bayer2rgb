precision highp float;

varying vec2 opos;
uniform sampler2D BayerTex;

void main(void) {
	gl_FragColor = texture2D(BayerTex, vec2(opos.x, opos.y));
	// gl_FragColor=vec4(r,g,b,1);
}

