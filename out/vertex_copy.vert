attribute vec3 position;
attribute vec2 texpos;
varying vec2 opos;
void main(void)
{
	opos = texpos;
	gl_Position = vec4(position, 1.0);
}
