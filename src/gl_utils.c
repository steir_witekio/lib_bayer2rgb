/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright (C) Witekio, 2017
 */


#include "gl_utils.h"

void print_opengl_error(const char *file, int line)
{
	GLenum err;

	err = glGetError();

	while (err != GL_NO_ERROR) {
		char error[128];

		switch (err) {
		case GL_INVALID_OPERATION:
			strcpy(error, "INVALID_OPERATION");
			break;
		case GL_INVALID_ENUM:
			strcpy(error, "INVALID_ENUM");
			break;
		case GL_INVALID_VALUE:
			strcpy(error, "INVALID_VALUE");
			break;
		case GL_OUT_OF_MEMORY:
			strcpy(error, "OUT_OF_MEMORY");
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			strcpy(error, "INVALID_FRAMEBUFFER_OPERATION");
			break;
		}

		printf("GL_%s - %s:%d\n", error, file, line);
		err = glGetError();
	}
}

int compile_shader(const char *FName, GLuint ShaderNum)
{
	FILE *fptr = NULL;
	fptr = fopen(FName, "rb");

	if (fptr == NULL) {
		printf("Cannot open file '%s'\n", FName);
		return -1;
	}

	int length;
	fseek(fptr, 0, SEEK_END);
	length = ftell(fptr);
	fseek(fptr, 0, SEEK_SET);

	char *shaderSource = malloc(sizeof(char) * (unsigned int)length);
	if (shaderSource == NULL) {
		printf("Out of memory.\n");
		return -1;
	}
	if (!fread(shaderSource, (size_t)length, 1, fptr)) {
		printf("fread error.\n");

		return -1;
	}

	glShaderSource(ShaderNum, 1, (const char * const*)&shaderSource, &length);
	glCompileShader(ShaderNum);

	free(shaderSource);
	fclose(fptr);

	GLint compiled = 0;
	glGetShaderiv(ShaderNum, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		/* Retrieve error buffer size. */
		GLint errorBufSize;
		GLsizei errorLength;
		glGetShaderiv(ShaderNum, GL_INFO_LOG_LENGTH, &errorBufSize);

		char *infoLog = malloc((unsigned int)errorBufSize * sizeof(char) + 1);
		if (infoLog) {
			/* Retrieve error. */
			glGetShaderInfoLog(ShaderNum, errorBufSize,
				 &errorLength, infoLog);
			infoLog[errorBufSize + 1] = '\0';
			printf("%s\n", infoLog);
			free(infoLog);
		}

		printf("Error compiling shader '%s'\n", FName);
		return -1;
	}

	return 0;
}

int load_shaders(const char *vShaderFName, const char *pShaderFName,
	GLuint *program_handle)
{
	GLuint pixelShaderNum;
	GLuint vertShaderNum;

	vertShaderNum = glCreateShader(GL_VERTEX_SHADER);
	pixelShaderNum = glCreateShader(GL_FRAGMENT_SHADER);

	if (compile_shader(vShaderFName, vertShaderNum) == -1) {
		return -1;
	}

	if (compile_shader(pShaderFName, pixelShaderNum) == -1) {
		return -1;
	}

	*program_handle = glCreateProgram();
	glAttachShader(*program_handle, vertShaderNum);
	glAttachShader(*program_handle, pixelShaderNum);
	glLinkProgram(*program_handle);

	/* Check if linking succeeded. */
	GLint linked = GL_FALSE;
	glGetProgramiv(*program_handle, GL_LINK_STATUS, &linked);
	if (!linked) {
		/* Retrieve error buffer size. */
		GLint errorBufSize, errorLength;
		glGetShaderiv(*program_handle, GL_INFO_LOG_LENGTH,
				&errorBufSize);

		char *infoLog = malloc((unsigned int)errorBufSize * sizeof(char) + 1);
		if (infoLog) {
			/* Retrieve error. */
			glGetProgramInfoLog(*program_handle, errorBufSize,
				  &errorLength, infoLog);
			infoLog[errorBufSize + 1] = '\0';
			printf("%s", infoLog);

			free(infoLog);
		}

		printf("Error linking program\n");
		return -1;
	}

	return 0;
}

void print_config_attributes(EGLDisplay display, EGLConfig config)
{
	EGLint value;
	printf("--------------------------------------------------------\n");

	eglGetConfigAttrib(display, config, EGL_CONFIG_ID, &value);
	printf("EGL_CONFIG_ID %d\n", value);
	eglGetConfigAttrib(display, config, EGL_BUFFER_SIZE, &value);
	printf("EGL_BUFFER_SIZE %d\n", value);
	eglGetConfigAttrib(display, config, EGL_RED_SIZE, &value);
	printf("EGL_RED_SIZE %d\n", value);
	eglGetConfigAttrib(display, config, EGL_GREEN_SIZE, &value);
	printf("EGL_GREEN_SIZE %d\n", value);
	eglGetConfigAttrib(display, config, EGL_BLUE_SIZE, &value);
	printf("EGL_BLUE_SIZE %d\n", value);
	eglGetConfigAttrib(display, config, EGL_ALPHA_SIZE, &value);
	printf("EGL_ALPHA_SIZE %d\n", value);
	eglGetConfigAttrib(display, config, EGL_DEPTH_SIZE, &value);
	printf("EGL_DEPTH_SIZE %d\n", value);
	eglGetConfigAttrib(display, config, EGL_STENCIL_SIZE, &value);
	printf("EGL_STENCIL_SIZE %d\n", value);
	eglGetConfigAttrib(display, config, EGL_SAMPLE_BUFFERS, &value);
	printf("EGL_SAMPLE_BUFFERS %d\n", value);
	eglGetConfigAttrib(display, config, EGL_SAMPLES, &value);
	printf("EGL_SAMPLES %d\n", value);

	eglGetConfigAttrib(display, config, EGL_CONFIG_CAVEAT, &value);
	switch (value)
	{
		case  EGL_NONE :
			printf("EGL_CONFIG_CAVEAT EGL_NONE\n");
			break;
		case  EGL_SLOW_CONFIG :
			printf("EGL_CONFIG_CAVEAT EGL_SLOW_CONFIG\n");
			break;
	}

	eglGetConfigAttrib(display, config, EGL_MAX_PBUFFER_WIDTH, &value);
	printf("EGL_MAX_PBUFFER_WIDTH %d\n", value);
	eglGetConfigAttrib(display, config, EGL_MAX_PBUFFER_HEIGHT, &value);
	printf("EGL_MAX_PBUFFER_HEIGHT %d\n", value);
	eglGetConfigAttrib(display, config, EGL_MAX_PBUFFER_PIXELS, &value);
	printf("EGL_MAX_PBUFFER_PIXELS %d\n", value);
	eglGetConfigAttrib(display, config, EGL_NATIVE_RENDERABLE, &value);
	printf("EGL_NATIVE_RENDERABLE %s \n", (value ? "true" : "false"));
	eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &value);
	printf("EGL_NATIVE_VISUAL_ID %d\n", value);
	eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_TYPE, &value);
	printf("EGL_NATIVE_VISUAL_TYPE 0x%02X\n", value);
	eglGetConfigAttrib(display, config, EGL_SURFACE_TYPE, &value);
	printf("EGL_SURFACE_TYPE 0x%02X\n", value);
	eglGetConfigAttrib(display, config, EGL_TRANSPARENT_TYPE, &value);
	printf("EGL_TRANSPARENT_TYPE 0x%02X\n", value);
}
