/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright (C) Witekio, 2017
 */


#include "gl_utils.h"
#include "init_window.h"
#include "init_window_log.h"
#include "utils.h"

/* Ctrl-C Handling */
static volatile int running = 1;

void ctrlc_handler() {
	running = 0;
}

/* Triangle Vertex positions */
const GLfloat vertices[][2] = {
	{-1.0f, -1.0f},
	{1.0f, -1.0f},
	{-1.0f, 1.0f},
	{1.0f, 1.0f}
};

const GLfloat texcoords[][2] = {
	{0.0f, 1.0f},
	{1.0f, 1.0f},
	{0.0f, 0.0f},
	{1.0f, 0.0f}
};

struct _escontext ESContext = {
	.native_display = NULL,
	.window_width = 0,
	.window_height = 0,
	.native_window  = 0,
	.display = NULL,
	.context = NULL,
	.surface = NULL
};

void *texture_buf;
void *data_buf;

GLuint framebuffer_object;

GLuint texture;
GLuint rendered_texture;

GLuint programHandle[2] = { 0, 0 };


EGLBoolean CreateEGLContext()
{
	EGLint numConfigs;
	EGLint majorVersion;
	EGLint minorVersion;
	EGLContext context;
	EGLSurface surface = EGL_NO_SURFACE;
	EGLConfig config;

	EGLint context_attribs[] = {
		EGL_CONTEXT_CLIENT_VERSION,  2,
		EGL_NONE,                    EGL_NONE
	};

	EGLint const fb_attribs[] =
	{
		EGL_SURFACE_TYPE,            EGL_PBUFFER_BIT,
		EGL_CONFORMANT,              EGL_OPENGL_ES2_BIT,
		EGL_RENDERABLE_TYPE,         EGL_OPENGL_ES2_BIT,
		EGL_COLOR_BUFFER_TYPE,       EGL_RGB_BUFFER,
		EGL_DEPTH_SIZE,              0,
		EGL_STENCIL_SIZE,            0,
		EGL_LUMINANCE_SIZE,          0,
		EGL_RED_SIZE,                8,
		EGL_GREEN_SIZE,              8,
		EGL_BLUE_SIZE,               8,
		EGL_ALPHA_SIZE,              0,
		EGL_BIND_TO_TEXTURE_RGBA,    EGL_TRUE,
		EGL_NONE, EGL_NONE
	};

	EGLint const surface_attribs[] =
	{
		EGL_WIDTH,                   WINDOW_WIDTH,
		EGL_HEIGHT,                  WINDOW_HEIGHT,
		EGL_LARGEST_PBUFFER,         32,
		EGL_NONE,                    EGL_NONE
	};

	EGLDisplay display = eglGetDisplay(ESContext.native_display);
	if (display == EGL_NO_DISPLAY)
	{
		printf("[ERROR] : No EGL Display...\n");
		return EGL_FALSE;
	}

	if (!eglInitialize(display, &majorVersion, &minorVersion))
	{
		printf("[ERROR] : No Initialisation...\n");
		return EGL_FALSE;
	}

	printf("EGL version: %d.%d\n", majorVersion, minorVersion);

	if ((eglGetConfigs(display, NULL, 0, &numConfigs) != EGL_TRUE) || (numConfigs == 0))
	{
		printf("[ERROR] : No configuration...\n");
		return EGL_FALSE;
	}

	if ((eglChooseConfig(display, fb_attribs, &config, 1, &numConfigs) != EGL_TRUE) || (numConfigs != 1))
	{
		printf("[ERROR] : No configuration...\n");
		return EGL_FALSE;
	}

	// print_config_attributes(display, config);

	// // Create a surface
	surface = eglCreatePbufferSurface(display, config, surface_attribs);
	if (surface == EGL_NO_SURFACE)
	{
		printf("[ERROR] : No surface...\n");
		return EGL_FALSE;
	}

	// Create a GL context
	context = eglCreateContext(display, config, EGL_NO_CONTEXT, context_attribs);
	if (context == EGL_NO_CONTEXT)
	{
		printf("[ERROR] : No context...\n");
		return EGL_FALSE;
	}

	// Make the context current
	if (!eglMakeCurrent(display, surface, surface, context))
	{
		printf("[ERROR] : Could not make the current window current !\n");
		return EGL_FALSE;
	}

	ESContext.display = display;
	ESContext.surface = surface;
	ESContext.context = context;
	return EGL_TRUE;
}


int draw() {

	int i;
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_object);
	PRINT_OGL_ERROR();

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, rendered_texture);
	PRINT_OGL_ERROR();

	// Give an empty image to OpenGL ( the last "0" )
	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, WINDOW_WIDTH, WINDOW_HEIGHT, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);
	PRINT_OGL_ERROR();

	// Poor filtering. Needed !
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	PRINT_OGL_ERROR();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	PRINT_OGL_ERROR();

	// Set "rendered_texture" as our colour attachement #0
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, rendered_texture, 0);
	PRINT_OGL_ERROR();




	/* Draw triangle. */
	glUseProgram(programHandle[0]);
	PRINT_OGL_ERROR();

	/* Draw background */
	GLfloat R = 0.3f, G = 1.0f, B = 1.0f, A = 1.0f;
	glClearColor(R, G, B, A);
	PRINT_OGL_ERROR();
	glClear(GL_COLOR_BUFFER_BIT);
	PRINT_OGL_ERROR();

	/* Y planar */
	glActiveTexture(GL_TEXTURE0);
	PRINT_OGL_ERROR();
	glBindTexture(GL_TEXTURE_2D, texture);
	PRINT_OGL_ERROR();

	/* Get BayerTex attribute to associate to TEXTURE0 */
	i = glGetUniformLocation(programHandle[0], "BayerTex");
	PRINT_OGL_ERROR();
	glUniform1i(i, 0);  /* Bind BayerTex to texture unit 0 */
	PRINT_OGL_ERROR();

	/* Y planar */
	glActiveTexture(GL_TEXTURE0);
	PRINT_OGL_ERROR();
	glBindTexture(GL_TEXTURE_2D, texture);
	PRINT_OGL_ERROR();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, WINDOW_WIDTH, WINDOW_HEIGHT, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, texture_buf);
	PRINT_OGL_ERROR();

	// /* Draw */
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	PRINT_OGL_ERROR();


	//after drawing
	glReadPixels(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA,
				GL_UNSIGNED_BYTE, data_buf);
	PRINT_OGL_ERROR();

	// Return to onscreen rendering:
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	PRINT_OGL_ERROR();

	glUseProgram(programHandle[1]);

	glBindTexture(GL_TEXTURE_2D, rendered_texture);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	/**********************
	*  DEBUG THE BUFFER  *
	**********************/

	if (((char *)data_buf)[0] != 0xFF ||
	    ((char *)data_buf)[1] != 0x00 ||
	    ((char *)data_buf)[2] != 0xFF ||
	    ((char *)data_buf)[3] != 0xFF)
	{
		printf("[ERROR] The buffer is not correct\n");
		hex_dump("buffer", data_buf, 12);
		return -1;
	}

	return 0;
}


void destroy_window() {

	glDeleteFramebuffers(1, &framebuffer_object);
	glDeleteTextures(1, &rendered_texture);
	glDeleteTextures(1, &texture);

	free(data_buf);
	free(texture_buf);

	eglMakeCurrent(ESContext.display, EGL_NO_SURFACE,
		EGL_NO_SURFACE, EGL_NO_CONTEXT);
	eglDestroySurface(ESContext.display, ESContext.surface);
	eglDestroyContext(ESContext.display, ESContext.context);
	eglTerminate(ESContext.display);
	glDeleteProgram (programHandle[0]);
	glDeleteProgram (programHandle[1]);
}


int main() {

	GLint locVertices;
	GLint locTexcoord;
	signal(SIGINT, ctrlc_handler);

	struct timeval t1, t2;
	struct timezone tz;
	float deltatime;
	float totaltime = 0.0f;
	unsigned int frames = 0;

	texture_buf = malloc(IMAGE_RESOLUTION);
	data_buf = malloc(IMAGE_RESOLUTION*4);

	printf("HELLO\n");

	if (!CreateEGLContext()) {
		exit(1);
	}

	load_shaders("vertex_copy.vert", "nv12_to_rgb.frag", &programHandle[0]);
	load_shaders("vertex_copy.vert", "simple.frag", &programHandle[1]);

	/*
	* Generate two textures: 1 for U planar and 2 for V planar
	* This is easier to handle in the shader for the conversion
	*/
	glGenTextures(1, &texture);

	/* Apply some filter on the texture */
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	/* Grab location of shader attributes. */
	locVertices = glGetAttribLocation(programHandle[0], "position");
	locTexcoord = glGetAttribLocation(programHandle[0], "texpos");

	/* Enable vertex arrays to push the data. */
	glEnableVertexAttribArray((GLuint)locVertices);
	glEnableVertexAttribArray((GLuint)locTexcoord);

	/* set data in the arrays. */
	glVertexAttribPointer((GLuint)locVertices, 2, GL_FLOAT, GL_FALSE, 0,
			&vertices[0][0]);
	glVertexAttribPointer((GLuint)locTexcoord, 2, GL_FLOAT, GL_FALSE, 0,
			&texcoords[0][0]);

	read_file("BAYER", texture_buf);

	//Somewhere at initialization
	glGenFramebuffers(1, &framebuffer_object);
	PRINT_OGL_ERROR();

	glGenTextures(1, &rendered_texture);

	gettimeofday (&t1, &tz);

	while (running) {

		gettimeofday(&t2, &tz);
		deltatime = (float)(t2.tv_sec - t1.tv_sec + (t2.tv_usec - t1.tv_usec) * 1e-6);
		t1 = t2;

		/******************
		*  DRAW/REFRESH  *
		******************/

		if (draw()) {
			break;
		}
		// eglSwapBuffers(ESContext.display, ESContext.surface);

		/*************************
		*  PERFORMANCE MEASURE  *
		*************************/

		totaltime += deltatime;
		frames++;
		if (totaltime >  2.0f)
		{
			printf("%4d frames rendered in %1.4f seconds -> FPS=%3.4f\n",
				frames, totaltime, (float)frames/totaltime);
			totaltime -= 2.0f;
			frames = 0;
		}
	}

	destroy_window();

	LOG("Display disconnected !\n");

	exit(0);
}
