/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Copyright (C) Witekio, 2017
 */


#include "utils.h"

void read_file(const char *filename, void *buffer) {
	int fd = 0;
	int n = 0;
	int size = 0;

	fd = open(filename, O_RDONLY);
	do {
		n = read(fd, (char *)buffer + size, (IMAGE_RESOLUTION) - (size_t)size);
		size += n;
	} while (size < IMAGE_RESOLUTION);
}

void hex_dump(char *name_buffer, void *buffer, int len) {

	int i;
	unsigned char buff[17];
	unsigned char *pc = (unsigned char*)buffer;

	/* output buffer if given */
	if (name_buffer != NULL)
		printf("%s:\n", name_buffer);

	if (len == 0) {
		printf("  ZERO LENGTH\n");
		return;
	}

	if (len < 0) {
		printf("  NEGATIVE LENGTH: %i\n", len);
		return;
	}
	printf("  ADDR  R  G  B  A\n");
	/* process every byte in the data */
	for (i = 0; i < len; i++) {
		/* multiple of 16 means new line (with line offset) */

		if ((i % 16) == 0) {
			/* just don't print ASCII for the zeroth line */
			if (i != 0)
				printf("  %s\n", buff);

			/* output the offset */
			printf("  %04x ", i);
		}

		/* now the hex code for the specific character */
		printf(" %02X", pc[i]);

		/* and store a printable ASCII character for later */
		if ((pc[i] < 0x20) || (pc[i] > 0x7e))
			buff[i % 16] = '.';
		else
			buff[i % 16] = pc[i];
		buff[(i % 16) + 1] = '\0';
	}

	/* pad out last line if not exactly 16 characters */
	while ((i % 16) != 0) {
		printf("   ");
		i++;
	}

	/* and print the final ASCII bit */
	printf("  %s\n", buff);
}

